module Mastaneh 
    # to be done 
    class Normalizer 
        def initialize(persian_string)
            @str = persian_string 
        end 
        # Fixing Arabic kaf
        def fix_arabic_kaf()
            @str = @str.tr("ك", "ک") 
        end 
        # Fixing Arabic ya
        def fix_arabic_ya()
            @str = @str.tr("ي", "ی") 
        end

        # Fixing Urdu ya
        def fix_urdu_ya()
            @str = @str.tr("ے", "ی")
        end 
        
        #Normalizes the string
        def normalize()
            fix_arabic_kaf(@str) 
            fix_arabic_ya(@str) 
        end 
    end 
end