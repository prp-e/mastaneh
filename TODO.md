# TODO List 

## Features 

| Feature | Status |
|:-------:|:------:|
|Tokenizer| Not done|
|Stop word filtering | Not done | 
|Sentiment analysis  | Not done |  
| Normalizer         | In progress | 

## Other things to be done 

| Task | Status | 
|:----:|:------:|
| Finding a labeled dataset | Not done | 
| Making a labeled dataset  | Not done | 
| Licensing the project     | Not done | 


